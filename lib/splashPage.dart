import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_with_mysql/loginPage.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    Timer(Duration(seconds: 3), () {
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=> LoginPage()
    ), (route) => false);
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  Container(
        width: double.infinity,
        color: Colors.teal,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:const [
            Text('Splash', style: TextStyle(fontSize: 30, color: Colors.white),),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 3,
              width: 200,
              child: LinearProgressIndicator(
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );

  }
}
