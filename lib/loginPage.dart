import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_with_mysql/homePage.dart';
import 'package:http/http.dart' as http;
class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController username=TextEditingController();
  TextEditingController password=TextEditingController();
  GlobalKey<FormState>? formKey;
  Future loginHelper()async{
  String url="http://192.168.0.105/flutter/login.php";
  //var url=await Client().get(Uri.parse(baseUrl));
  var response=await http.post(Uri.parse(url),body: {
    "username":username.text,
    "password":password.text,
  });
  var data=json.decode(response.body);
  print(data);
  if(data=="success")
    {
      Navigator.push(context, MaterialPageRoute(builder: (context)=>  HomePage()));
    }
  }
  @override
  void initState() {
    super.initState();
    formKey= GlobalKey<FormState>();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login Page'),
      ),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    controller: username,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Please enter valid UserId";
                      } else {
                        return null;
                      }
                    },
                    style: const TextStyle(fontSize: 20),
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.email_outlined),
                      hintText: 'Email',
                      filled: true,
                      fillColor: Colors.grey.shade300,
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.grey,
                        ),
                      ),
                      border: const OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    controller: password,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Please enter valid UserId";
                      } else {
                        return null;
                      }
                    },
                    style: const TextStyle(fontSize: 20),
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.lock),
                      hintText: 'Password',
                      filled: true,
                      fillColor: Colors.grey.shade300,
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.grey,
                        ),
                      ),
                      border: const OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text('Forget Password?',
                    style: TextStyle(
                      color: Colors.blue,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    )),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child:TextButton(
                    onPressed: (){
                      loginHelper();
                    },
                    child:  Text('Login'),
                  )
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
